package com.example.practica0392_java;

public class calculadora {

    // Atributos
    private float numero1;
    private float numero2;


    // Constructores
    public calculadora(){
        this.numero1 = 0.0f;
        this.numero2 = 0.0f;
    }
    public calculadora(float numero1, float numero2) {
        this.numero1 = numero1;
        this.numero2 = numero2;
    }
    public calculadora(calculadora c) {
        this.numero1 = c.numero1;
        this.numero2 = c.numero2;
    }


    // Metodos - Operaciones
    public float suma(){
        return numero1 + numero2;
    }
    public float resta(){
        return numero1 - numero2;
    }
    public float multiplicacion(){
        return numero1 * numero2;
    }
    public float divicion(){
        if(numero2 != 0)
            return numero1 / numero2;
        else
            return  0;
    }


    // ENCAPSULAMIENTO
    public float getNumero1() {
        return numero1;
    }
    public void setNumero1(float numero1) {
        this.numero1 = numero1;
    }
    public float getNumero2() {
        return numero2;
    }
    public void setNumero2(float numero2) {
        this.numero2 = numero2;
    }
}

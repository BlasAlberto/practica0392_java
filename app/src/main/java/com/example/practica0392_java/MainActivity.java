package com.example.practica0392_java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Declaración de componentes
    private EditText txtUsuario;
    private EditText txtContraseña;
    private Button btnSalir;
    private Button btnIngresar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.iniciarComponentes();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingresar();
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salir();
            }
        });
    }


    // Relacionar componentes con los del Layout
    public void iniciarComponentes(){
        this.txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        this.txtContraseña = (EditText) findViewById(R.id.txtContraseña);
        this.btnSalir = (Button) findViewById(R.id.btnSalir);
        this.btnIngresar = (Button) findViewById(R.id.btnIngresar);
    }

    // Metodo clic para el Button : btnIngresar
    public void ingresar(){
        String strUsuario = getString(R.string.usuario);
        String strContraseña = getString(R.string.contraseña);

        if(txtUsuario.getText().toString().equals(strUsuario)
        && txtContraseña.getText().toString().equals(strContraseña)){

            // Crear paquete para enviar información
            Bundle bundle = new Bundle();
            // Agregar String(usuario) al paquete
            bundle.putString("Usuario", txtUsuario.getText().toString());

            // Crear intent para llamar otra actividad
            Intent intento = new Intent(MainActivity.this, actCalculadora.class);
            // Pasar el Bundle creado a la actuvidad destino
            intento.putExtras(bundle);
            // Iniciar la actividad esperando o no respuesta
            startActivity(intento);

            // Limpiar los campos
            this.txtUsuario.setText("");
            this.txtContraseña.setText("");
        }
        else Toast.makeText(this, "El usuario o la contraseña son incorrectos", Toast.LENGTH_SHORT).show();
    }

    // Metodo clic para el Button : btnSalir
    public void salir(){
        // Crear configuraciones de ventana emergente
        AlertDialog.Builder config = new AlertDialog.Builder(this);
        config.setTitle("Calculadora");
        config.setMessage("¿Decea cerrar la aplicación?");

        // Asignación de botones
        config.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        config.setNegativeButton("Cancelar", null);

        // Crear, asignar configuraciones y mostrar ventana emergente
        AlertDialog confirmacion = config.create();
        confirmacion.show();
    }
}
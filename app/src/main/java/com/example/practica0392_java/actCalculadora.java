package com.example.practica0392_java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class actCalculadora extends AppCompatActivity {

    // Declaración de componentes
    private TextView lblUsuario;
    private TextView lblResultado;
    private EditText txtNumero1;
    private EditText txtNumero2;
    private Button btnSuma;
    private Button btnResta;
    private Button btnMultiplicacion;
    private Button btnDivicion;
    private Button btnLimpiar;
    private Button btnRegresar;

    // Declaración de otros objetos
    private calculadora calcular;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_calculadora);

        this.iniciarComponentes();
        this.asignarEventos();

        this.calcular = new calculadora();

        // Obtener datos del Intent
        Intent intent = getIntent();
        String strUsuario = intent.getStringExtra("Usuario");
        this.lblUsuario.setText(strUsuario);
    }


    // Relacionar conponentes con los del Layout
    private void iniciarComponentes(){
        this.lblUsuario = (TextView) findViewById(R.id.lblUsuario);
        this.lblResultado = (TextView) findViewById(R.id.lblResultado);

        this.txtNumero1 = (EditText) findViewById(R.id.txtNum1);
        this.txtNumero2 = (EditText) findViewById(R.id.txtNum2);

        this.btnSuma = (Button) findViewById(R.id.btnSumar);
        this.btnResta = (Button) findViewById(R.id.btnRestar);
        this.btnMultiplicacion = (Button) findViewById(R.id.btnMultiplicar);
        this.btnDivicion = (Button) findViewById(R.id.btnDividir);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresar);
    }


    // Asignar metodos a los Buttons
    private void asignarEventos(){
        this.btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicSumar();
            }
        });
        this.btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicRestar();
            }
        });
        this.btnMultiplicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicMultiplicar();
            }
        });
        this.btnDivicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicDivicion();
            }
        });
        this.btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicRegresar();
            }
        });
        this.btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicLimpiar();
            }
        });

    }


    // Metodos de calculo
    private void clicSumar(){
        String num1 = txtNumero1.getText().toString();
        String num2 = txtNumero2.getText().toString();

        if(!num1.equals("") && !num2.equals("")){

            calcular.setNumero1(Float.parseFloat(num1));
            calcular.setNumero2(Float.parseFloat(num2));

            lblResultado.setText(String.valueOf(calcular.suma()));
        }
        else Toast.makeText(this, "Todos los campos son necesarios.", Toast.LENGTH_SHORT).show();
    }
    private void clicRestar(){
        String num1 = txtNumero1.getText().toString();
        String num2 = txtNumero2.getText().toString();

        if(!num1.equals("") && !num2.equals("")){

            calcular.setNumero1(Float.parseFloat(num1));
            calcular.setNumero2(Float.parseFloat(num2));

            lblResultado.setText(""+calcular.resta());
        }
        else Toast.makeText(this, "Todos los campos son necesarios.", Toast.LENGTH_SHORT).show();
    }
    private void clicMultiplicar(){
        String num1 = txtNumero1.getText().toString();
        String num2 = txtNumero2.getText().toString();

        if(!num1.equals("") && !num2.equals("")){

            calcular.setNumero1(Float.parseFloat(num1));
            calcular.setNumero2(Float.parseFloat(num2));

            lblResultado.setText(""+calcular.multiplicacion());
        }
        else Toast.makeText(this, "Todos los campos son necesarios.", Toast.LENGTH_SHORT).show();
    }
    private void clicDivicion(){
        String num1 = txtNumero1.getText().toString();
        String num2 = txtNumero2.getText().toString();

        if(!num1.equals("") && !num2.equals("")){

            calcular.setNumero1(Float.parseFloat(num1));
            calcular.setNumero2(Float.parseFloat(num2));

            lblResultado.setText(""+calcular.divicion());
        }
        else Toast.makeText(this, "Todos los campos son necesarios.", Toast.LENGTH_SHORT).show();
    }

    private void clicRegresar(){
        // Crear configuraciones de ventana emergente
        AlertDialog.Builder config = new AlertDialog.Builder(this);
        config.setTitle("Calculadora");
        config.setMessage("¿Decea cerrar seción?");

        // Asignación de botones
        config.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        config.setNegativeButton("Cancelar", null);

        // Crear, asignar configuraciones y mostrar ventana emergente
        AlertDialog confirmacion = config.create();
        confirmacion.show();
    }

    private void clicLimpiar(){
        this.txtNumero1.setText("");
        this.txtNumero2.setText("");
        this.lblResultado.setText("Resultado");
    }
}